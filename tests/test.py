def test_os_release(host):
    assert host.file("/etc/os-release").contains("Alpine")

def test_nginx_release(host):
    nginx = host.package("nginx")
    assert nginx.is_installed
    assert nginx.version.startswith("1.16.1")